package com.example.demo.controllers

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
open class WebConfig : WebMvcConfigurer {

    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.addViewController("/").setViewName("home")
        registry.addViewController("/home").setViewName("home")
        registry.addViewController("/index").setViewName("home")
        registry.addViewController("/index.html").setViewName("home")
        registry.addViewController("/error").setViewName("error")
    }
}
