package com.example.demo

import com.example.demo.data.StudentRepository
import com.example.demo.models.Student
import com.example.demo.utils.Log
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
open class Application {

    @Bean
    open fun dataLoader(studentRepository: StudentRepository): CommandLineRunner {
        Log.info("dataLoader")
        return CommandLineRunner {
            studentRepository.save(Student(null, "Anton", "Zaviyalov", 29))
            studentRepository.save(Student(null, "Mike", "Tyson", 31))
            studentRepository.save(Student(null, "Jack", "Sparrow", 19))
        }
    }

}

fun main(args: Array<String>) {
    Log.info("application is started")
    SpringApplication.run(Application::class.java, *args)
}