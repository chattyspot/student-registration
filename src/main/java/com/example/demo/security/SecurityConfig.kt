package com.example.demo.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.crypto.password.StandardPasswordEncoder
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder

@Configuration
@ComponentScan("com.example.demo")
@EnableWebSecurity
open class SecurityConfig : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
                .antMatchers("/info/**").hasAnyRole("ADMIN", "ROLE_USER")
                .and()
                .formLogin()

    }

//    @Autowired
//    private lateinit var userDetailsService: UserDetailsService
//
//    override fun configure(http: HttpSecurity) {
//        http.authorizeRequests()
//                .antMatchers(
//                        "/home",
//                        "/all-students",
//                        "/new-student",
//                        "save-student",
//                        "student-saved"
//                )
//                .access("hasRole('ROLE_USER')")
//                .antMatchers("/", "/**")
//                .access("permitAll")
//
//                .and()
//                .formLogin()
//                .loginPage("login")
//
//                .and()
//                .logout()
//                .logoutSuccessUrl("/")
//
//                .and()
//                .csrf()
//                .ignoringAntMatchers("/h2-console/**")
//
//                .and()
//                .headers()
//                .frameOptions()
//                .sameOrigin()
//    }

    @Bean
    open fun encoder(): PasswordEncoder = StandardPasswordEncoder("aksfdj")

    // in memory
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password("admin")
                .authorities("ADMIN")
                .and()
                .withUser("zavanton")
                .password("1234")
                .authorities("ROLE_USER")

    }
}