package com.example.demo.controllers

import com.example.demo.data.StudentRepository
import com.example.demo.models.Student
import com.example.demo.utils.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/save-student")
@SessionAttributes("student")
class ConfirmSaveStudentController
@Autowired
constructor(private val studentRepository: StudentRepository) {

    @GetMapping
    fun showPassedStudent(@ModelAttribute("student") student: Student): String {
        Log.info("showPassedStudent")
        Log.info("The received student: $student")
        return "save-student"
    }

    @PostMapping
    fun saveStudent(@ModelAttribute("student") student: Student): String {
        Log.info("saveStudent: $student")
        val saved = studentRepository.save(student)
        Log.info("saved student: $saved")
        return "redirect:/student-saved"
    }
}