package com.example.demo.models

import javax.persistence.*
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "Student")
data class Student(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @field:NotEmpty(message = "Must not be empty!")
        var firstName: String? = null,

        @field:NotEmpty(message = "Must not be empty!")
        var lastName: String? = null,

        @field:Min(value = 17L, message = "Student must be at least 17 years old!")
        var age: Int = 0
)
