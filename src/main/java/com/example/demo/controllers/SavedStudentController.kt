package com.example.demo.controllers

import com.example.demo.models.Student
import com.example.demo.utils.Log
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.SessionAttributes
import org.springframework.web.bind.support.SessionStatus

@Controller
@RequestMapping("/student-saved")
@SessionAttributes("student")
class SavedStudentController {

    @GetMapping
    fun showSavedStudent(
            @ModelAttribute("student")
            student: Student,
            sessionStatus: SessionStatus
    ): String {
        Log.info("showSavedStudent: $student")
        Log.info("sessionStatus - before: ${sessionStatus.isComplete}")
        sessionStatus.setComplete()
        Log.info("sessionStatus - after: ${sessionStatus.isComplete}")
        return "student-saved"
    }
}