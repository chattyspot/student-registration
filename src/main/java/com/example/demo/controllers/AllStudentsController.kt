package com.example.demo.controllers

import com.example.demo.data.StudentRepository
import com.example.demo.utils.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("all-students")
class AllStudentsController
@Autowired
constructor(private val studentRepository: StudentRepository) {

    @GetMapping
    fun showAllStudents(model: Model): String {
        Log.info("showAllStudents")

        val students = studentRepository.findAll()
        model.addAttribute("students", students)

        return "all-students"
    }
}