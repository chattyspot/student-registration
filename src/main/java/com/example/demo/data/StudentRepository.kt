package com.example.demo.data

import com.example.demo.models.Student

import org.springframework.data.repository.CrudRepository

interface StudentRepository : CrudRepository<Student, Long>
