package com.example.demo.controllers

import com.example.demo.models.Student
import com.example.demo.utils.Log
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.Errors
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Controller
@RequestMapping("/new-student")
@SessionAttributes("student")
class NewStudentController {

    @GetMapping
    fun showCreateStudentForm(model: Model): String {
        Log.info("showCreateStudentForm")
        val student = Student(null, "Galina", "Zavyalova", 30)
        model.addAttribute("student", student)
        return "new-student"
    }

    @PostMapping
    fun processStudent(@Valid @ModelAttribute("student") student: Student,
                       errors: Errors,
                       model: Model): String {
        Log.info("processStudent")
        if (errors.hasErrors()) {
            return "new-student"
        }

        Log.info("The student to pass: $student")
        return "redirect:/save-student"
    }
}