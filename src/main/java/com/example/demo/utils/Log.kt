package com.example.demo.utils

import com.example.demo.Application
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private val log: Logger = LoggerFactory.getLogger(Application::class.java)

object Log {

    fun info(message: String) {
        log.info(message)
    }
}